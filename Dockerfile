FROM ubuntu:18.04

ARG FOLDER=dist

#update && install
RUN apt-get update &&\
    echo "y" | apt-get install apache2 &&\
    rm -r /var/www/html

WORKDIR /var/www

#cp dist /var/www/html
COPY ${FOLDER} html

EXPOSE 80

#start apache2
CMD ["apachectl", "-D", "FOREGROUND"]
