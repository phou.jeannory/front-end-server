import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { RoleGuardService } from 'app/services/role-guard.service';
import { HomeComponent } from 'app/home/home.component';
import { SpamComponent } from 'app/spam/spam.component';

export const AdminLayoutRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'spam',
        component: SpamComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
    {
        path: 'user-profile',
        component: UserProfileComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
    {
        path: 'table-list',
        component: TableListComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['manager']
        }
    },
    {
        path: 'typography',
        component: TypographyComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
    {
        path: 'icons',
        component: IconsComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
    {
        path: 'maps',
        component: MapsComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
    {
        path: 'notifications',
        component: NotificationsComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
    {
        path: 'upgrade',
        component: UpgradeComponent,
        canActivate: [RoleGuardService],
        data: {
            expectedRole: ['user', 'manager']
        }
    },
];
