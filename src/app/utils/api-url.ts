import { environment } from '../../environments/environment';

export class ApiUrl {
    private static readonly USER_SERVICE_BASE_URL = `http://${environment.PROXY_SERVICE_BASE_URL}:${environment.PROXY_SERVICE_PORT}/user-service`;
    private static readonly BASE_API = '/api';
    private static readonly USERS = '/users';
    private static readonly CONNECTED_USER = '/connected-user';
    private static readonly AUTH = '/auth/';
    private static readonly AUTH_SERVER = `http://${environment.AUTH_BASE_URL}:${environment.AUTH_PORT}`;
    private static readonly SPAM = '/spam/get-spam';
    private static readonly BASE_SPAM_MODULE = `http://${environment.SERVER_MODULE}:${environment.SPAM_APP_PORT}`;
    private static readonly SPAM_MESSAGE = '/spam-app';

    static get GET_CONNECTED_USER(): string {
        return `${this.USER_SERVICE_BASE_URL}${this.BASE_API}${this.USERS}${this.CONNECTED_USER}`;
    }

    static get GET_USERS(): string {
        return `${this.USER_SERVICE_BASE_URL}${this.BASE_API}${this.USERS}`;
    }

    static get GET_AUTH_SERVER(): string {
        return `${this.AUTH_SERVER}${this.AUTH}`;
    }

    static get GET_SPAM(): string {
        return `${this.USER_SERVICE_BASE_URL}${this.BASE_API}${this.SPAM}`;
    }

    static get GET_SPAM_MESSAGE(): string {
        return `${this.BASE_SPAM_MODULE}${this.SPAM_MESSAGE}`;
    }
}
