import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, DoBootstrap, ApplicationRef } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
/**
import {
  AgmCoreModule
} from '@agm/core';
*/
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { KeycloakSecurityService } from './services/keycloak-security.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptorService } from './services/request-interceptor.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const securityService = new KeycloakSecurityService();

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    /**
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
    */
    HttpClientModule,
    MatSnackBarModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
  ],
  providers: [
    { provide: KeycloakSecurityService, useValue: securityService },
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorService, multi: true }
  ],
})

export class AppModule implements DoBootstrap {

  ngDoBootstrap(appRef: ApplicationRef): void {
    securityService.init()
      .then(res => {
        console.log(res);
        appRef.bootstrap(AppComponent);
      })
      .catch((err) => {
        console.log('Keycloak error ', err);
      });
  }
}
