import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-spam',
  templateUrl: './spam.component.html',
  styleUrls: ['./spam.component.css']
})
export class SpamComponent implements OnInit {

  spam: any;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  spamManager() {
    this.apiService.getSpam()
      .subscribe(data => {
        this.spam = data;
        console.log('Now : ' + this.spam.now + 'is connected : ' + this.spam.connected);
      }, err => {
        console.log('err.error.message : ' + err.error.message);
      })
  }

}
