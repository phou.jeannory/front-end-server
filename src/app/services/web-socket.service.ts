import { Injectable } from '@angular/core';
import { ApiUrl } from '../utils/api-url';
declare var SockJS;
declare var Stomp;
@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  apiUrl = ApiUrl;
  public stompClient;
  public msg = [];

  constructor() {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    const serverUrl = this.apiUrl.GET_SPAM_MESSAGE;
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/message', (message) => {
        if (message.body) {
          that.msg.push(message.body);
        }
      });
    });
  }
}
