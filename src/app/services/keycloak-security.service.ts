import { Injectable } from '@angular/core';
import { ApiUrl } from '../utils/api-url';

declare var Keycloak: any;
@Injectable({
  providedIn: 'root'
})
export class KeycloakSecurityService {

  public kc;
  apiUrl = ApiUrl;

  constructor() { }
  init() {
    return new Promise((resolve, reject) => {
      console.log('security Initialisation ...');
      this.kc = new Keycloak({
        url: this.apiUrl.GET_AUTH_SERVER,
        realm: 'Sc-project',
        clientId: 'sc-ui'
      });
      this.kc.init({
        onLoad: 'check-sso',
        promiseType: 'native'
      }).then((authenticated) => {
        console.log(this.kc.token);
        resolve({ auth: authenticated, token: this.kc.token });
        sessionStorage.setItem('token', this.kc.token);
      }).catch(err => {
        reject(err);
      });
    });
  }

}