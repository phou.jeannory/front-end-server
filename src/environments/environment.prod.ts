// import { version } from '../../package.json';

export const environment = {
  // server-module
  SERVER_MODULE: '35.205.152.152',
  // spam-module
  SPAM_APP_PORT: '8083',
  // my-app-back-end
  PROXY_SERVICE_BASE_URL: '35.208.214.143',
  PROXY_SERVICE_PORT: '9999',
  // keycloak server
  AUTH_BASE_URL: 'jeannory.ovh',
  AUTH_PORT: '8099',
  // APP_VERSION: version,
  production: true,
};
