// import { version } from '../../package.json';
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  // server-module
  SERVER_MODULE: 'localhost',
  // spam-module
  SPAM_APP_PORT: '8083',
  PROXY_SERVICE_BASE_URL: 'localhost',
  PROXY_SERVICE_PORT: '9999',
  AUTH_BASE_URL: 'localhost',
  AUTH_PORT: '8099',
  // APP_VERSION: version,
  production: false,
};
